package com.company;

import java.io.*;

class PipedStreamDemo {
    // поток производитель данных
    private static class Producer extends Thread {
        // поток вывода любого типа
        private DataOutputStream dos;
        public Producer(OutputStream os) {
            dos = new DataOutputStream(os);
        }
        @Override
        public void run() {
            // что-то выводим в поток вывода
            String s = null;
            try {
                for (int i = 0; i < 5; ++i) {
                    s = i + ": Hello, world";
                    System.out.println("produce string " + s);
                    dos.writeUTF(s);

                    // засыпаем на 1 секунду для проверки ожидания потребителя
                    if (i == 10) {
                        Thread.sleep(1000);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            System.out.println("end of producer thread");
        }
    }
    // поток потребитель данных
    private static class Consumer extends Thread {
        // поток ввода любого типа
        private DataInputStream dis;
        public Consumer(InputStream is) {
            dis = new DataInputStream(is);
        }
        @Override
        public void run() {
            // принимаем данные
            try {
                for (int i = 0; i < 5; ++i) {
                    System.out.println("consume string " + dis.readUTF());
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            System.out.println("end of consumer thread");
        }
    }
    public static void main(String[] args) {
        try {
            PipedOutputStream pos = new PipedOutputStream();
            PipedInputStream pis = new PipedInputStream(pos);
            Producer producer = new Producer(pos);
            Consumer consumer = new Consumer(pis);
            producer.start();
            consumer.start();
            producer.join();
            consumer.join();
            System.out.println("end of application");
            pos.close();
            pis.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}